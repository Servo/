# Generated by Django 3.2.2 on 2021-05-15 12:14

from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('servo', '0003_auto_20210515_1333'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attachment',
            name='ref',
        ),
        migrations.RemoveField(
            model_name='attachment',
            name='ref_id',
        ),
        migrations.AddField(
            model_name='attachment',
            name='content_type',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='attachment',
            name='object_id',
            field=models.PositiveIntegerField(default=None),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='queue',
            name='locations',
            field=models.ManyToManyField(blank=True, help_text='Pick the locations you want this queue to appear in.', to='servo.Location', verbose_name='locations'),
        ),
        migrations.AlterField(
            model_name='user',
            name='customer',
            field=mptt.fields.TreeForeignKey(blank=True, help_text='Tie the user to this customer (company).', limit_choices_to={'is_company': True}, null=True, on_delete=django.db.models.deletion.SET_NULL, to='servo.customer'),
        ),
    ]
