# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from servo.tasks import check_mail


class Command(BaseCommand):
    help = "Checks IMAP box for new mail"

    def handle(self, *args, **options):
        check_mail()
