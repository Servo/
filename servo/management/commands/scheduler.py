from datetime import datetime

import django
import django_rq


from django_rq.management.commands import rqscheduler

#django.setup() # To avoid django.core.exceptions.AppRegistryNotReady: Apps aren't loaded yet.

from servo.tasks import check_mail


scheduler = django_rq.get_scheduler('default')


class Command(rqscheduler.Command):

    help = "Start task scheduler"

    def handle(self, *args, **kwargs):
        # reset all tasks on registration
        for job in scheduler.get_jobs():
            job.delete()

        scheduler.schedule(
            scheduled_time=datetime.utcnow(), # Time for first execution, in UTC timezone
            func=check_mail, # Function to be queued
            interval=300, # Time before the function is called again, in seconds
        )

        super(Command, self).handle(*args, **kwargs)
